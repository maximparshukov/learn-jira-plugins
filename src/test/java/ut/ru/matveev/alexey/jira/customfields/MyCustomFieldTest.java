package ut.ru.matveev.alexey.jira.customfields;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import ru.matveev.alexey.jira.customfields.MyCustomField;

import static org.mockito.Mockito.*;

/**
 * @since 3.5
 */
public class MyCustomFieldTest {

    @Before
    public void setup() {

    }

    @After
    public void tearDown() {

    }

    @Test(expected=Exception.class)
    public void testSomething() throws Exception {

        //MyCustomField testClass = new MyCustomField();

        throw new Exception("MyCustomField has no tests!");

    }

}
