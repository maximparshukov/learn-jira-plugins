package ru.matveev.alexey.jira.webwork;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import ru.matveev.alexey.jira.service.PluginSettingService;
import webwork.action.ResultException;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.Set;

@Slf4j
@Data
public class MyWebworkAction extends JiraWebActionSupport
{
    private String parameter1;
    private String parameter2;
    private final PluginSettingService pluginSettingService;


    @Inject
    public MyWebworkAction(PluginSettingService pluginSettingService) {
        this.pluginSettingService = pluginSettingService;
        this.refreshParameters();

    }

    @Override
    public String doExecute() throws Exception {
        this.refreshParameters();
        return INPUT; //returns SUCCESS
    }

    public String doSave() throws ResultException {
        this.validate();
        this.pluginSettingService.setParameter1(this.parameter1);
        this.pluginSettingService.setParameter2(this.parameter2);
        this.refreshParameters();
        return INPUT;
    }

    @Override
    public void doValidation() {
        try {
            Integer param1 = Integer.parseInt(this.parameter1);
            if (param1 < 1) {
                throw new NumberFormatException();
            }
        } catch(NumberFormatException e) {
            this.errorMap = new HashMap();
            this.errorMap.put("parameter1", "not positive integer");
        }
    }

    public String doClear() {
        return SUCCESS;
    }

    private void refreshParameters() {
        this.parameter1 = this.pluginSettingService.getParameter1();
        this.parameter2 = this.pluginSettingService.getParameter2();
    }
}
